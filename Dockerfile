FROM zout84/askbot-docker

MAINTAINER fluendo 
# Install required packages 
RUN apt-get update && \
	apt-get install -y \ 
		python \ 
		python-dev \ 
		python-setuptools \ 
		python-pip \ 
		python-psycopg2 \
		nginx \ 
		supervisor \ 
		sqlite3 && \ 
		pip install -U pip setuptools 
		
# install uwsgi now because it takes a little while 
RUN pip install uwsgi 

# Make NGINX run on the foreground 
RUN echo "daemon off;" >> /etc/nginx/nginx.conf 

# Remove default configuration from Nginx 
RUN rm /etc/nginx/sites-enabled/default

# Copy the modified Nginx conf 
COPY nginx.conf /etc/nginx/sites-enabled/ 

# Copy the base uWSGI ini file to enable default dynamic uwsgi process number 
COPY uwsgi.ini /etc/uwsgi/ 

# Custom Supervisord config 
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf 

# askbot config files
COPY askbot.conf /etc/nginx/conf.d/askbot.conf

RUN mkdir -p /etc/uwsgi/sites
COPY askbot.ini /etc/uwsgi/sites/

COPY uwsgi.service /etc/systemd/system/

# db connection
COPY settings.py /app/


# Which uWSGI .ini file should be used, to make it customizable 

EXPOSE 80 

# Define default command. 
CMD ["/app/deploy/run.sh"]

